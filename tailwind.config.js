/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        pink: '#D30975',
        pink80: '#D30975CC'
      }
    }
  },
  plugins: []
}
