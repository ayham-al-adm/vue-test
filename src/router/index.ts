import { createRouter, createWebHistory } from 'vue-router'
import MainLayout from '@/layouts/MainLayout.vue'
import HomeScreenLayout from '@/layouts/HomeScreenLayout.vue'
import { useAuthStore } from '@/stores/auth'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      meta: { layout: HomeScreenLayout },
      component: () => import('@/views/HomeView.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: { layout: MainLayout },
      component: () => import('@/views/LoginView.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: { layout: MainLayout },
      component: () => import('@/views/RegisterView.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore()

  authStore.verifyAuth()

  // before page access check if page requires authentication
  if (to.meta.middleware == 'auth') {
    if (authStore.isAuthenticated) {
      next()
    } else {
      next({ name: 'login' })
    }
  } else {
    next()
  }
})

export default router
