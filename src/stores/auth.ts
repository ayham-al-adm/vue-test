import { ref } from 'vue'
import { defineStore } from 'pinia'
import ApiService from '@/services/ApiService'
import JwtService from '@/services/JwtService'

export interface User {
  fname: string
  lname: string
  email: string
  password: string
  phone: string
  referralCode: string
}

export const useAuthStore = defineStore('auth', () => {
  const errors = ref({})
  const user = ref<User>({} as User)
  const isAuthenticated = ref(!!JwtService.getToken())

  function setAuth(authUser: any) {
    isAuthenticated.value = true
    user.value = authUser.user
    errors.value = {}
    JwtService.saveToken(authUser.token)
  }

  function setError(error: any) {
    errors.value = { ...error }
  }

  function purgeAuth() {
    isAuthenticated.value = false
    user.value = {} as User
    errors.value = []
    JwtService.destroyToken()
  }

  async function login(credentials: User) {
    const payload = { ...credentials, _method: 'POST' }

    return ApiService.post('api/users/login', payload)
      .then(({ data }) => {
        setAuth(data.data)
      })
      .catch(({ response }) => {
        setError(response.data.errors || [response.data.message])
      })
  }

  function logout() {
    purgeAuth()
  }

  function register(credentials: User) {
    return ApiService.post('api/users/register', credentials)
      .then(({ data }) => {
        setAuth(data)
      })
      .catch(({ response }) => {
        setError(response.data.message)
      })
  }

  function forgotPassword(email: string) {
    return ApiService.post('forgot_password', email)
      .then(() => {
        setError({})
      })
      .catch(({ response }) => {
        setError(response.data.errors)
      })
  }

  function verifyAuth() {
    // TODO: check if token still unexpired
  }

  return {
    errors,
    user,
    isAuthenticated,
    login,
    logout,
    register,
    forgotPassword,
    verifyAuth
  }
})
