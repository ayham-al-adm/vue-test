import { ref } from 'vue'
import { doFilter } from './pureHomeScreenFilters'

// Imperative Shell
export function useHomeScreenFilters() {
  const results = ref([])
  const from = ref(1)
  const to = ref(3)
  const leaveOn = ref(null)
  const returnOn = ref(null)
  const flightClass = ref(null)

  const submitFilterHandler = () => {
    // Using the functional core
    results.value = doFilter()
  }

  // TODO: handle changes
  const changeFromHandler = () => {
    console.log('changed')
  }
  const changeToHandler = () => {}
  const changeLeaveOnHandler = () => {}
  const changeReturningOnHandler = () => {}
  const changeFlightClassHandler = () => {}

  return {
    results,
    from,
    to,
    leaveOn,
    returnOn,
    flightClass,
    submitFilterHandler,
    changeFromHandler,
    changeToHandler,
    changeLeaveOnHandler,
    changeReturningOnHandler,
    changeFlightClassHandler
  }
}
