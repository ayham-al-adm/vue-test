import type { App } from 'vue'
import type { AxiosResponse } from 'axios'
import axios from 'axios'
import VueAxios from 'vue-axios'
import JwtService from '@/services/JwtService'

class ApiService {
  public static vueInstance: App

  public static init(app: App<Element>) {
    ApiService.vueInstance = app
    ApiService.vueInstance.use(VueAxios, axios)
    ApiService.vueInstance.axios.defaults.baseURL = import.meta.env.VITE_APP_API_URL
  }

  public static setHeader(): void {
    ApiService.vueInstance.axios.defaults.headers.common['Authorization'] =
      `Bearer ${JwtService.getToken()}`
    ApiService.vueInstance.axios.defaults.headers.common['Accept'] = 'application/json'
  }

  public static query(resource: string, params: any): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.get(resource, params)
  }

  public static get(resource: string, slug = '' as string): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.get(`${resource}` + (slug ? `/${slug}` : ''))
  }

  public static post(resource: string, params: any): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.post(`${resource}`, params, { method: 'POST' })
  }

  public static update(resource: string, slug: string, params: any): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.put(`${resource}/${slug}`, params)
  }

  public static put(resource: string, params: any): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.put(`${resource}`, params)
  }

  public static delete(resource: string): Promise<AxiosResponse> {
    return ApiService.vueInstance.axios.delete(resource)
  }

  public static setStaticToken(token: string): void {
    ApiService.vueInstance.axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  }
}

export default ApiService
